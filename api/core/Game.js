const GameDictionary = require('./GameDictionary');
const ArrayHelper = require('./ArrayHelper');
const WordClassifier = require('./WordClassifier');

module.exports = {
  tiles: 4,
  minWordLength: 3,

  /* False if no added words are missing */
  canFinish(playStatus) {
    return playStatus.addedWords === 0;
  },

  /* Valid when provided chars are more or equal to board grid */
  validateAmountOfChars(amount) {
    return amount >= this.tiles * this.tiles;
  },

  /* Validate if word is valid for game */
  validateGameWord(word) {
    return word.length >= this.minWordLength;
  },

  /* Validate raw position (number between 0 and 15) */
  validateRawPosition(position) {
    return position >= 0 && position < this.tiles * this.tiles;
  },

  /* It generates a new game status with a random generated board using words */
  generateNew(words) {
    const boardData = this.populateWithWords(
      0,
      WordClassifier.groupByLengthAndLetters(words),
      this.emptyGameBoard(),
      this.randomGameBoardPosition(),
      0,
      this.tiles * this.tiles,
      null
    );

    const currentBoard = this.emptyGameBoard();

    /* New game status */
    return { gameBoard: boardData.board, currentBoard: currentBoard, addedWords: boardData.addedWords };
  },

  /* It validates a new played word using the gameStatus, dictionary words, already played words and the proposed positions */
  validatePlay(gameStatus, dictionaryWords, playedWords, positions) {
    const gameBoard = gameStatus.gameBoard;
    const currentBoard = gameStatus.currentBoard;
    const addedWords = gameStatus.addedWords;
    const playedWord = this.followMovements(gameBoard, positions);
    const gameDictionary = GameDictionary.new(dictionaryWords);
    const playedDictionary = GameDictionary.new(playedWords);

    if (GameDictionary.has(playedDictionary, playedWord)) {
      // Word alredy played
      return this.playResponse(gameBoard, currentBoard, addedWords, false, 'Word already played', 0);
    } else if (GameDictionary.has(gameDictionary, playedWord)) {
      return this.performPlay(
        gameBoard,
        currentBoard,
        addedWords,
        positions,
        this.scoreWord(playedWord),
        playedWord
      );
    } else {
      // Word is not inside dictionary
      return this.playResponse(gameBoard, currentBoard, addedWords, false, 'Word is not inside dictionary', 0);
    }
  },

  // Build the played word
  followMovements(gameBoard, positions) {
    var letters = [];

    for (var index = 0; index < positions.length; index++) {
      const position = positions[index];
      const playedLetter = this.getFromBoard(gameBoard, position);
      letters.push(playedLetter);
    }

    return letters.join('');
  },

  performPlay(gameBoard, currentBoard, addedWords, positions, score, playedWord) {
    var placedLetters = 0;

    for (var index = 0; index < positions.length; index++) {
      const position = positions[index];
      const playedLetter = this.getFromBoard(gameBoard, position);

      if (this.positionIsEmpty(currentBoard, position)) {
        placedLetters++;
        this.putInBoard(currentBoard, position, playedLetter);
      }
    }

    if (placedLetters === 0) {
      return this.playResponse(gameBoard, currentBoard, addedWords, false, 'Already played', 0);
    } else {
      return this.playResponse(gameBoard, currentBoard, addedWords - 1, true, '', score, playedWord);
    }
  },

  playResponse(gameBoard, currentBoard, addedWords, status, message, score, playedWord) {
    return {
      gameBoard: gameBoard,
      currentBoard: currentBoard,
      addedWords: addedWords,
      valid: status,
      message: message,
      score: score,
      playedWord: playedWord
    };
  },

  /* Convert plain number to board position */
  toGamePosition(position) {
    return {
      i: Math.floor(position / this.tiles),
      j: Math.floor(position % this.tiles)
    };
  },

  /* Main recursive function in charge of populate board with words */
  populateWithWords(addedWords, wordsByLength, gameBoard, wordPosition, filledSpaces, maxSpaces, nextWord) {
    if (filledSpaces < maxSpaces) {
      var word;
      var boardAndNextPosition;
      var nextWordsByLength;
      var nextPosition;

      if (nextWord === null) {
        wordLength = WordClassifier.getRandomMapKey(wordsByLength);
        word = WordClassifier.getRandomWordWithLength(wordsByLength, wordLength);
      } else {
        word = nextWord;
        wordLength = word.length;
      }

      boardAndNextPosition = this.placeWordInsideBoard(gameBoard, wordPosition, word, wordLength, 0);

      // Uncomment to watch steps when building board:
      // console.log(word);
      // this.printBoard(boardAndNextPosition.gameBoard);

      // When a word is added, check randomly if starting another one in the same point
      if (boardAndNextPosition.addedLength === wordLength) {
        addedWords++;
        nextWordsByLength = WordClassifier.removeWord(wordsByLength, word, wordLength);
        const lastLetter = word.slice(0, -1);

        if (this.flipCoin() && WordClassifier.wordsStartingWith(wordsByLength, lastLetter)) {
          nextPosition = wordPosition;
          nextWord = WordClassifier.getRandomWordWithLetter(wordsByLength, lastLetter);
        } else {
          nextPosition = boardAndNextPosition.wordPosition;
          nextWord = null;
        }
      } else {
        nextWordsByLength = wordsByLength;
        nextPosition = boardAndNextPosition.wordPosition;
        nextWord = null;
      }

      return this.populateWithWords(
        addedWords,
        nextWordsByLength,
        boardAndNextPosition.gameBoard,
        nextPosition,
        filledSpaces + boardAndNextPosition.addedLength,
        maxSpaces,
        nextWord
      );
    } else {
      return { board: gameBoard, addedWords: addedWords };
    }
  },

  /* Initializes a new empty game board */
  emptyGameBoard() {
    var gameBoard = {};

    for (let i = 0; i < this.tiles; i++) {
      for (let j = 0; j < this.tiles; j++) {
        gameBoard[i] = gameBoard[i] || {};
        gameBoard[i][j] = null;
      }
    }

    return gameBoard;
  },

  /* Get an empty space, closest to top left corner */
  getUpperEmptySpace(gameBoard) {
    for (let i = 0; i < this.tiles; i++) {
      for (let j = 0; j < this.tiles; j++) {
        if (gameBoard[i][j] === null) {
          return { i: i, j: j };
        }
      }
    }
    return false;
  },

  /* Get any random position from the game board */
  randomGameBoardPosition() {
    randomI = Math.floor(Math.random() * this.tiles);
    randomJ = Math.floor(Math.random() * this.tiles);

    return { continue: true, position: { i: randomI, j: randomJ } };
  },

  /* Compute next position for word letter, if possible */
  nextGamePosition(gameBoard, wordPosition) {
    const position = wordPosition.position;
    const positions = this.availableNextGamePosition(gameBoard, position);

    if (positions.length > 0) {
      return { continue: true, position: ArrayHelper.getRandomElement(positions) };
    } else {
      return { continue: false, position: this.getUpperEmptySpace(gameBoard) };
    }
  },

  /* Computes all the available neighbor positions */
  availableNextGamePosition(gameBoard, position) {
    var positions = [];

    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        if (i === 0 && j === 0) {
          continue;
        }

        const currentI = i + position.i;
        const currentJ = j + position.j;
        const column = gameBoard[currentI];

        if (column !== undefined && column[currentJ] === null) {
          positions.push({ i: currentI, j: currentJ });
        }
      }
    }

    return positions;
  },

  /* Puts a word inside the board, if possible */
  placeWordInsideBoard(gameBoard, wordPosition, word, wordLength, addedLength) {
    const firstLetter = word.slice(0, 1);
    this.putInBoard(gameBoard, wordPosition.position, firstLetter);
    addedLength++;
    wordLength--;
    const nextGamePosition = this.nextGamePosition(gameBoard, wordPosition);

    if (nextGamePosition.continue && wordLength > 0) {
      return this.placeWordInsideBoard(
        gameBoard,
        nextGamePosition,
        word.slice(1),
        wordLength,
        addedLength
      );
    } else {
      return { gameBoard: gameBoard, wordPosition: nextGamePosition, addedLength: addedLength };
    }
  },

  /* 50% random helper */
  flipCoin() {
    Math.floor(Math.random() * 1) > 0;
  },

  /* Prints the board, just for debugging */
  printBoard(gameBoard) {
    for (let i = 0; i < this.tiles; i++) {
      var row = [];
      for (let j = 0; j < this.tiles; j++) {
        row.push(this.getFromBoard(gameBoard, { i: i, j: j }) || '[E]');
      }
      console.log(row.join('\t'));
    }
  },

  putInBoard(board, position, letter) {
    board[position.i][position.j] = letter;
  },

  getFromBoard(board, position) {
    return board[position.i][position.j];
  },

  getBoardRow(board, position) {
    return board[position.i];
  },

  positionIsEmpty(board, position) {
    return this.getFromBoard(board, position) === null;
  },

  scoreWord(word) {
    return Math.pow(2, word.length - 3);
  },

  toArrayBoard(board) {
    let rows = [];

    for (let i = 0; i < this.tiles; i++) {
      let row = [];

      for (let j = 0; j < this.tiles; j++) {
        row.push(this.getFromBoard(board, { i: i, j: j }) || '[x]');
      }
      rows.push(row);
    }

    return rows;
  }
};

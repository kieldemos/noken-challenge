module.exports = {
  /* Get a random elemnt from array */
  getRandomElement(array) {
    var randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
  }
};

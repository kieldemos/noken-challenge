module.exports = {
  new(words) {
    dictionary = new Map();

    for (var index = 0; index < words.length; index++) {
      const word = words[index];
      dictionary.set(word, true);
    }

    return dictionary;
  },

  has(dictionary, word) {
    return dictionary.has(word) === true;
  }
};

const ArrayHelper = require('./ArrayHelper');

module.exports = {
  /* Build a nested Map to classify words by length and first letter */
  groupByLengthAndLetters(words) {
    var wordsByLength = new Map();

    for (let i = 0; i < words.length; i++) {
      const word = words[i];
      const len = word.length;
      const firstLetter = word.slice(0, 1);

      var letterMap = wordsByLength.get(len);

      if (letterMap === undefined) {
        letterMap = new Map();
        letterMap.set(firstLetter, [word]);
      } else {
        var letterArray = letterMap.get(firstLetter);

        if (letterArray === undefined) {
          letterMap.set(firstLetter, [word]);
        } else {
          letterArray.push(word);
          letterMap.set(firstLetter, letterArray);
        }
      }

      wordsByLength.set(len, letterMap);
    }

    return wordsByLength;
  },

  /* Remove a word, and shrinks the structure when possible */
  removeWord(wordsByLength, word, wordLength) {
    const firstLetter = word.slice(0, 1);
    var wordLevel = wordsByLength.get(wordLength);
    var wordsArray = wordLevel.get(firstLetter);

    const index = wordsArray.indexOf(word);
    wordsArray.splice(index, 1);

    if (wordsArray.length === 0) {
      wordLevel.delete(firstLetter);
    } else {
      wordLevel.set(firstLetter, wordsArray);
    }

    if (Array.from(wordLevel.keys()).length === 0) {
      wordsByLength.delete(wordLength);
    } else {
      wordsByLength.set(wordLength, wordLevel);
    }

    return wordsByLength;
  },

  /* Get a random word, starting with 'firstLetter' param */
  getRandomWordWithLetter(wordsByLength, firstLetter) {
    var letterWords = [];

    for (const level in wordsByLength) {
      if (level.has(firstLetter)) {
        letterWords.push(level.get(firstLetter));
      }
    }

    return ArrayHelper.getRandomElement(letterWords);
  },

  /* Get a random word of length 'wordLength' param */
  getRandomWordWithLength(wordsByLength, wordLength) {
    const byLength = wordsByLength.get(wordLength);
    const randomLetterLevel = byLength.get(this.getRandomMapKey(byLength));

    return ArrayHelper.getRandomElement(randomLetterLevel);
  },

  /* Get a random key for the provided map, minor helper */
  getRandomMapKey(map) {
    return ArrayHelper.getRandomElement(Array.from(map.keys()));
  },

  /* True when at least having one word starting with 'lastLetter' param */
  wordsStartingWith(wordsByLength, lastLetter) {
    for (const lengthLevel in wordsByLength.values()) {
      if (lengthLevel.has(lastLetter)) {
        return true;
      }
    }

    return false;
  }
};

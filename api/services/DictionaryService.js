const Game = require('../core/Game');

module.exports = {
  /* Search for one dictionary record, by id */
  async findOne(dictionaryId) {
    var dictionaryRecord = await Dictionary.findOne(dictionaryId).populate('words');

    if (dictionaryRecord) {
      return ServiceResponse.success(this.toCustomJSON(dictionaryRecord));
    } else {
      return ServiceResponse.notFound('Dictionary was not found.');
    }
  },

  /* Search all dictionaries, using pagination */
  async find(name, page) {
    var skip = (page - 1) * 30;
    var results;

    if (name !== null && name !== '') {
      results = await Dictionary.find({ where: { name: name }, skip: skip, limit: 30, sort: 'id DESC' }).populate('words');
    } else {
      results = await Dictionary.find({ skip: skip, limit: 30, sort: 'id DESC' }).populate('words');
    }

    var dictionaries = [];
    for (const it in results) {
      if (results.hasOwnProperty(it)) {
        const dictionary = results[it];
        dictionaries.push(this.toCustomJSON(dictionary));
      }
    }

    return ServiceResponse.success(dictionaries);
  },

  /* Create a new dictionary with given words */
  async createDictionary(name, words) {
    var validationStatus = this.validateWords(words);

    if (validationStatus.isValid) {
      var expandedWords = this.expandWords(words);

      // Creating or finding words
      var wordIds = await this.createOrFindWordIds(expandedWords);

      // Creating new dictionary
      var dictionary = await Dictionary.create({ name: name }).fetch();

      // Binding word with dictionary
      wordIds.forEach(async wordId => {
        await DictionaryWord.create({ word: wordId, dictionary: dictionary.id });
      });

      // Reloading updated dictionary
      dictionary = await Dictionary.findOne({where: {id: dictionary.id}}).populate('words');

      return ServiceResponse.created(this.toCustomJSON(dictionary));
    } else {
      return ServiceResponse.unprocessable(validationStatus.errors);
    }
  },

  async createOrFindWordIds(words) {
    var wordIds = [];

    for (let index = 0; index < words.length; index++) {
      const word = words[index];
      var wordRecord = await WordService.findOrCreate(word.content, word.length, word.firstLetter);
      wordIds.push(wordRecord.id);
    }

    return wordIds;
  },

  expandWords(words) {
    var expandedWords = [];

    for (var index = 0; index < words.length; index++) {
      const word = words[index].toUpperCase();
      expandedWords.push({ content: word, length: word.length, firstLetter: word.slice(0, 1) });
    }

    return expandedWords;
  },

  toCustomJSON(dictionary) {
    var wordRecords = dictionary.words;

    var words = [];

    for (let index = 0; index < wordRecords.length; index++) {
      const word = wordRecords[index];
      words.push(word.content);
    }

    dictionary.words = words;

    return dictionary;
  },

  validateWords(words) {
    var totalChars = 0;
    var validationStatus = { isValid: true, errors: [] };

    for (var index = 0; index < words.length; index++) {
      const word = words[index];
      totalChars = totalChars + word.length;

      if (!Game.validateGameWord(word)) {
        validationStatus.isValid = false;
        validationStatus.errors.push('Word <' + word + '> is not valid.');
      }
    }

    if (words.length === 0) {
      validationStatus.isValid = false;
      validationStatus.errors.push('At least one word is required.');
    }

    if (!Game.validateAmountOfChars(totalChars)) {
      validationStatus.isValid = false;
      validationStatus.errors.push('More words are needed in order to fullfill board.');
    }

    return validationStatus;
  }
};

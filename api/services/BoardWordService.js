module.exports = {
  async findBoardWordsForPlayer(board, player) {
    return await BoardWord.find({where: {boardId: board.id, playerId: player.id }}).populate('word');
  },

  async addWord(board, player, word, positions) {
    const wordRecord = await WordService.find(word);

    return await BoardWord.create({
      board: board.id,
      player: player.id,
      word: wordRecord.id,
      positions: positions
    }).fetch();
  }
};

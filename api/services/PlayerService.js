module.exports = {
  findOrCreate: async function(username) {
    var playerRecord = await Player.findOne({username: username});

    if (playerRecord) {
      return playerRecord;
    } else {
      return await Player.create({
        username: username
      }).fetch();
    }
  }
};

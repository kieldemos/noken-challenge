module.exports = {
  async find(board, player) {
    return await BoardPlayer.findOne({where: { board: board.id, player: player.id }});
  },

  async create(board, player) {
    return await BoardPlayer.create({
      player: player.id,
      board: board.id,
      score: 0
    }).fetch();
  },

  async updateScore(board, player, score) {
    const currentBoardPlayer = await BoardPlayer.findOne({ where: { board: board.id, player: player.id }});

    return await BoardPlayer.updateOne({ where: { board: board.id, player: player.id }}).set({
      score: currentBoardPlayer.score + score
    });
  },

  async searchWinner(board) {
    return (await BoardPlayer.find({ where: { board: board.id }, sort: 'score DESC', limit: 1 }).populate('player'))[0];
  }
};

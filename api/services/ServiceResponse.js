module.exports = {
  success(data) {
    return { data: data, statusCode: 200 };
  },

  created(data) {
    return { data: data, statusCode: 201 };
  },

  unauthorized(errors) {
    return { errors: errors, statusCode: 401 };
  },

  notFound(errors) {
    return { errors: errors, statusCode: 404 };
  },

  unprocessable(errors) {
    return { errors: errors, statusCode: 422 };
  },

  isError(response) {
    return response.statusCode !== 200 && response.statusCode !== 201;
  },

  getData(response) {
    return response.data;
  },

  processResponse(response, exits) {
    switch (response.statusCode) {
      case 200:
        return exits.success(response.data);
      case 201:
        return exits.created(response.data);
      case 401:
        return exits.unauthorized(response.errors);
      case 404:
        return exits.notFound(response.errors);
      case 422:
        return exits.unprocessableEntity(response.errors);

      default:
        return exits.unexpectedError(response.errors || 'Unexpected server error');
    }
  }
};

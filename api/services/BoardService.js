const Game = require('../core/Game');
const ArrayHelper = require('../core/ArrayHelper');

module.exports = {
  /* Find a board and add score info */
  async findOneWithScore(boardId) {
    var boardRecord =
      await Board.findOne(boardId)
        .populate('players')
        .populate('nextPlayer')
        .populate('boardPlayers')
        .populate('winnerPlayer');

    if (boardRecord) {
      return ServiceResponse.success(this.toCustomJSON(boardRecord));
    } else {
      return ServiceResponse.notFound('Board was not found.');
    }
  },

  /* Creates a new board providing a dictionary, a list of usernames and the gameStatus board */
  async createBoard(dictionary, usernames) {
    var gameStatus = Game.generateNew(dictionary.words);
    var validationStatus = this.validatePlayers(usernames);

    if (!validationStatus.isValid) {
      return ServiceResponse.unprocessable(validationStatus.errors);
    }

    var expandedPlayers = await this.expandPlayers(usernames);

    // Creating new board
    var boardRecord = await Board.create({
      dictionary: dictionary.id,
      status: 'new',
      winnerPlayer: null,
      nextPlayer: ArrayHelper.getRandomElement(expandedPlayers).id,
      gameStatus: gameStatus
    }).fetch();

    // Creating board players
    expandedPlayers.forEach(async player => {
      await BoardPlayerService.create(boardRecord, player);
    });

    // Reloading game record
    boardRecord =
      await Board.findOne({ where: { id: boardRecord.id } })
        .populate('players')
        .populate('nextPlayer')
        .populate('boardPlayers')
        .populate('winnerPlayer');

    return ServiceResponse.created(this.toCustomJSON(boardRecord));
  },

  /* Check if a play is valid, and updates game status */
  async playInBoard(boardId, player, positions) {
    var board = await Board.findOne(boardId).populate('players').populate('nextPlayer');

    var dictionary = await DictionaryService.findOne(board.dictionary);
    dictionary = ServiceResponse.getData(dictionary);

    if (!board) {
      return ServiceResponse.notFound('Board was not found.');
    }

    if (this.boardFinished(board)) {
      return ServiceResponse.unprocessable('Board already finished. Cannot play more.');
    }

    if (!this.allowedNextPlayer(board, player)) {
      return ServiceResponse.unauthorized('Player is not allowed to perform next play.');
    }

    var validationStatus = this.validatePositions(positions);

    if (!validationStatus.isValid) {
      return ServiceResponse.unprocessable(validationStatus.errors);
    }

    var wordPositions = this.toGamePositions(positions);
    var dictionaryWords = dictionary.words;
    var playedWords = await BoardWordService.findBoardWordsForPlayer(board, board.nextPlayer);
    var simplifiedPlayedWords = this.simplifiedPlayerWords(playedWords);
    var playStatus = Game.validatePlay(board.gameStatus, dictionaryWords, simplifiedPlayedWords, wordPositions);

    if (playStatus.valid) {
      // Play is valid, so we update all the game status
      const newBoardRecord = await this.updateGameStatus(board, playStatus, positions);
      return ServiceResponse.success(this.toCustomJSON(newBoardRecord));
    } else {
      // Play is not valid, so we update only the next player
      var newBoardRecord = await this.skipForNextPlayer(board, playStatus);
      newBoardRecord.errors = playStatus.message;

      return ServiceResponse.success(this.toCustomJSON(newBoardRecord));
    }
  },

  /* Updates the entire game status, with a new discovered word */
  async updateGameStatus(board, playStatus, positions) {
    const newGameStatus = {
      gameBoard: playStatus.gameBoard,
      addedWords: playStatus.addedWords,
      currentBoard: playStatus.currentBoard
    };

    await BoardWordService.addWord(board, board.nextPlayer, playStatus.playedWord, positions);
    await BoardPlayerService.updateScore(board, board.nextPlayer, playStatus.score);

    if (Game.canFinish(playStatus)) {
      const winnerBoardPlayer = await BoardPlayerService.searchWinner(board);

      return await this.saveNewStatus(board, newGameStatus, 'finished', winnerBoardPlayer.player.id);
    } else {
      return await this.saveNewStatus(board, newGameStatus, 'new', null);
    }
  },

  /* Updates only next player for board */
  async skipForNextPlayer(board, playStatus) {
    const newGameStatus = {
      gameBoard: playStatus.gameBoard,
      addedWords: playStatus.addedWords,
      currentBoard: playStatus.currentBoard
    };

    return await this.saveNewStatus(board, newGameStatus, 'new', null);
  },

  /* Save and retrieve the last board version */
  async saveNewStatus(board, newGameStatus, status, winnerPlayerId) {
    // Upadting board status
    await Board.updateOne({ id: board.id }).set({
      winnerPlayer: winnerPlayerId,
      status: status,
      gameStatus: newGameStatus,
      nextPlayer: this.findNextPlayer(board).id
    });

    // Getting updated board state with loaded elements
    return await Board.findOne(board.id)
      .populate('players')
      .populate('nextPlayer')
      .populate('boardPlayers')
      .populate('winnerPlayer');
  },

  /* Find the next player id, using players array order */
  findNextPlayer(board) {
    const currentPlayerId = board.nextPlayer.id;
    const amountOfPlayers = board.players.length;

    for (let index = 0; index < amountOfPlayers; index++) {
      const player = board.players[index];

      if (player.id === currentPlayerId) {
        return board.players[(index + 1) % amountOfPlayers];
      }
    }

    return null;
  },

  /* Build an array with player records finding them using their usernames */
  async expandPlayers(usernames) {
    var expandedPlayers = [];

    for (let index = 0; index < usernames.length; index++) {
      const username = usernames[index];
      var playerRecord = await PlayerService.findOrCreate(username);
      expandedPlayers.push(playerRecord);
    }

    return expandedPlayers;
  },

  /* Validate player usernames */
  validatePlayers(usernames) {
    var validationStatus = { isValid: true, errors: [] };

    for (var index = 0; index < usernames.length; index++) {
      const username = usernames[index];

      if (username.length < 4) {
        validationStatus.isValid = false;
        validationStatus.errors.push('Username <' + username + '> is not valid.');
      }
    }

    if (usernames.length < 2) {
      validationStatus.isValid = false;
      validationStatus.errors.push('At least two username are required.');
    }

    return validationStatus;
  },

  /* Validate provided board positions */
  validatePositions(positions) {
    var validationStatus = { isValid: true, errors: [] };

    for (var index = 0; index < positions.length; index++) {
      const position = positions[index];

      if (!Game.validateRawPosition(position)) {
        validationStatus.isValid = false;
        validationStatus.errors.push('Position <' + position + '> is not valid.');
      }
    }

    if (positions.length < 3) {
      validationStatus.isValid = false;
      validationStatus.errors.push('At least 3 positions are required to perform a valid play.');
    }

    return validationStatus;
  },

  boardFinished(board) {
    return board.status === 'finished';
  },

  allowedNextPlayer(board, player) {
    return board.nextPlayer.username === player;
  },

  toGamePositions(positions) {
    var wordPositions = [];

    for (let index = 0; index < positions.length; index++) {
      const position = positions[index];
      wordPositions.push(Game.toGamePosition(position));
    }

    return wordPositions;
  },

  simplifiedPlayerWords(boardWords) {
    var words = [];

    for (let index = 0; index < boardWords.length; index++) {
      const boardWord = boardWords[index];
      words.push(boardWord.word.content);
    }

    return words;
  },

  toArrayBoard(board) {
    return Game.toArrayBoard(board);
  },

  toCustomJSON(board) {
    var playersMap = new Map();
    var playerRecords = board.players;
    var players = [];

    for (let index = 0; index < playerRecords.length; index++) {
      const player = playerRecords[index];
      players.push(player.username);
      playersMap.set(player.id, player.username);
    }

    board.players = players;
    board.nextPlayer = board.nextPlayer.username;

    var playerScores = {};

    for (let index = 0; index < board.boardPlayers.length; index++) {
      const boardPlayer = board.boardPlayers[index];
      const username = playersMap.get(boardPlayer.player);
      playerScores[username] = boardPlayer.score;
    }

    delete (board.boardPlayers);
    board.score = playerScores;

    board.dictionaryId = board.dictionary;
    delete (board.dictionary);

    board.missingWords = board.gameStatus.addedWords;
    delete (board.gameStatus.addedWords);

    board.board = this.toArrayBoard(board.gameStatus.gameBoard);
    board.discoveredBoard = this.toArrayBoard(board.gameStatus.currentBoard);
    delete (board.gameStatus);

    if (board.winnerPlayer) {
      board.winnerPlayer = board.winnerPlayer.username;
    }

    return board;
  }
};

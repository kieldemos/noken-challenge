module.exports = {
  find: async function(content) {
    return await Word.findOne({content: content});
  },

  findOrCreate: async function(content, length, firstLetter) {
    var wordRecord = await Word.findOne({content: content});

    if (wordRecord) {
      return wordRecord;
    } else {
      return await Word.create({
        content: content,
        length: length,
        firstLetter: firstLetter
      }).fetch();
    }
  }
};

module.exports = function unauthorized(messages) {
  var res = this.res;
  res.status(401);
  return res.json({errors: messages});
};

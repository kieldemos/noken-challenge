module.exports = function created(data) {
  var res = this.res;
  res.status(201);
  return res.json(data);
};

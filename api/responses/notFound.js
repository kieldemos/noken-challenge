module.exports = function notFound(messages) {
  var res = this.res;
  res.status(404);
  return res.json({errors: messages});
};

module.exports = function success(data) {
  var res = this.res;
  res.status(200);
  return res.json(data);
};

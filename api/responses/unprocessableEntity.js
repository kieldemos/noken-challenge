module.exports = function unprocessableEntity(messages) {
  var res = this.res;
  res.status(422);
  return res.json({errors: messages});
};

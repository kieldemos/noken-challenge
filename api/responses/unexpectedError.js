module.exports = function unexpectedError(messages) {
  var res = this.res;
  res.status(500);
  return res.json({errors: messages});
};

/**
 * Dictionary.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: { type: 'string', required: true },

    dictionaryWords: {
      collection: 'DictionaryWord',
      via: 'dictionary'
    },

    words: {
      collection: 'Word',
      via: 'dictionary',
      through: 'DictionaryWord'
    },

    boards: {
      collection: 'Board',
      via: 'dictionary'
    }

  },

};


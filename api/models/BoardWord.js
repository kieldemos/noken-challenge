/**
 * BoardWord.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    board: { model: 'Board' },

    player: { model: 'Player' },

    word: { model: 'Word' },

    positions: { type: 'json' }

  },

};


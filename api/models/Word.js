/**
 * Word.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    content: { type: 'string', required: true },

    length: { type: 'number', required: true },

    firstLetter: { type: 'string', required: true },

    dictionaryWords: {
      collection: 'DictionaryWord',
      via: 'word'
    },

    dictionaries: {
      collection: 'Dictionary',
      via: 'word',
      through: 'DictionaryWord'
    },

    boardWords: {
      collection: 'BoardWord',
      via: 'word'
    }

  },

};


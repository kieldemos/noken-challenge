/**
 * Board.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    dictionary: { model: 'Dictionary' },

    status: { type: 'string' },

    gameStatus: { type: 'json' },

    winnerPlayer: { model: 'Player' },

    nextPlayer: { model: 'Player' },

    boardWords: {
      collection: 'BoardWord',
      via: 'board'
    },

    boardPlayers: {
      collection: 'BoardPlayer',
      via: 'board'
    },

    players: {
      collection: 'Player',
      via: 'board',
      through: 'BoardPlayer'
    }
  },
};


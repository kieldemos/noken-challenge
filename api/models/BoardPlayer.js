/**
 * BoardPlayer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    board: { model: 'Board' },

    player: { model: 'Player' },

    score: { type: 'number', defaultsTo: 0 }

  },

};


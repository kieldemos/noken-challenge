/**
 * Player.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    username: { type: 'string' },

    playedBoards: {
      collection: 'BoardPlayer',
      via: 'player'
    },

    wonBoards: {
      collection: 'Board',
      via: 'winnerPlayer'
    },

    nextBoards: {
      collection: 'Board',
      via: 'nextPlayer'
    },

    boardWords: {
      collection: 'BoardWord',
      via: 'player'
    },

    boardPlayers: {
      collection: 'BoardPlayer',
      via: 'player'
    },

    boards: {
      collection: 'Board',
      via: 'player',
      through: 'BoardPlayer'
    }

  },

};


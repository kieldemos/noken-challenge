module.exports = {
  friendlyName: 'Game board score',

  description: 'List current game status and score.',

  inputs: {
    boardId: {
      description: 'The board ID',
      type: 'number',
      required: true
    }
  },

  exits: {
    success: {
      description: 'Displaying a game board score',
      statusCode: 200,
      responseType: 'success'
    },

    notFound: {
      description: 'Game board not found',
      statusCode: 404,
      responseType: 'notFound'
    }
  },

  fn: async function ({boardId}, exits) {
    const boardResponse = await BoardService.findOneWithScore(boardId);

    if (ServiceResponse.isError(boardResponse)) {
      return ServiceResponse.processResponse(boardResponse, exits);
    }

    return ServiceResponse.processResponse(
      boardResponse,
      exits
    );
  }
};

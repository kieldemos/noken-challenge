module.exports = {
  friendlyName: 'Board Creation',

  description: 'Create a new board with a random dictionary distribution.',

  inputs: {
    dictionaryId: {
      description: 'The dictionary ID',
      type: 'number',
      required: true
    },

    players: {
      description: 'Players that participates in the game',
      type: ['string'],
      required: true
    }
  },

  exits: {
    created: {
      description: 'Board successfully created',
      statusCode: 201,
      responseType: 'created'
    },

    notFound: {
      description: 'Dictionary used to build Board was not found.',
      statusCode: 404
    },

    unprocessableEntity: {
      description: 'Invalid parameters to create a Board.',
      statusCode: 422,
      responseType: 'unprocessableEntity'
    }
  },

  fn: async function ({ dictionaryId, players }, exits) {
    var response = await DictionaryService.findOne(dictionaryId);

    if (ServiceResponse.isError(response)) {
      return ServiceResponse.processResponse(response, exits);
    }

    var dictionary = ServiceResponse.getData(response);

    return ServiceResponse.processResponse(
      await BoardService.createBoard(dictionary, players),
      exits
    );
  }
};

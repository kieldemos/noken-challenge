module.exports = {
  friendlyName: 'Play inside a board',

  description: 'Play next movement for board if board is not finished, and if your are the next player.',

  inputs: {
    boardId: {
      description: 'The board ID',
      type: 'number',
      required: true
    },

    player: {
      description: 'The player username',
      type: 'string',
      required: true
    },

    positions: {
      description: 'Players list of positions. Can be any number between 0 and 15',
      type: ['number'],
      required: true
    }
  },

  exits: {
    success: {
      description: 'Play successfully processed.',
      statusCode: 200,
      responseType: 'success'
    },

    unauthorized: {
      description: 'Not allowed to perform next play for Board.',
      statusCode: 401,
      responseType: 'unauthorized'
    },

    unprocessableEntity: {
      description: 'Invalid parameters or cannot play more on this Board.',
      statusCode: 422,
      responseType: 'unprocessableEntity'
    }
  },

  fn: async function ({ boardId, player, positions }, exits) {
    return ServiceResponse.processResponse(
      await BoardService.playInBoard(
        boardId,
        player,
        positions
      ),
      exits
    );
  }
};

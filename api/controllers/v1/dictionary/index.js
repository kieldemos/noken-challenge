module.exports = {
  friendlyName: 'List of dictionaries',

  description: 'List all created dictionaries.',

  inputs: {
    name: {
      description: 'Filter by dictionary name',
      type: 'string',
      required: false
    },
    page: {
      description: 'Page of results',
      type: 'number',
      required: false,
      default: 1
    }
  },

  exits: {
    success: {
      description: 'Displaying a list of dictionaries',
      statusCode: 200,
      responseType: 'success'
    }
  },

  fn: async function ({name, page}, exits) {
    return ServiceResponse.processResponse(
      await DictionaryService.find(name || null, page || 1),
      exits
    );
  }
};

module.exports = {
  friendlyName: 'New Dictionary',

  description: 'Creates a new dictionary providing a list of words.',

  inputs: {
    name: {
      description: 'The dictionary name',
      type: 'string',
      required: true
    },

    words: {
      description: 'A list of words',
      required: true,
      type: ['string']
    }
  },

  exits: {
    created: {
      description: 'Dictionary successfully created',
      statusCode: 201,
      responseType: 'created'
    },

    unprocessableEntity: {
      description: 'Invalid parameters to create a Dictionary.',
      statusCode: 422,
      responseType: 'unprocessableEntity'
    }
  },

  fn: async function ({ name, words }, exits) {
    return ServiceResponse.processResponse(
      await DictionaryService.createDictionary(name, words),
      exits
    );
  }
};

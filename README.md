# Solution for Noken Backend Challenge v1.0.1

## Implemented features

### API design. What is using this application?
- Boilerplate code was generated by [Sails JS v1.2.4](https://sailsjs.com/).
- The core logic is stored inside `api/core` folder.
- The modules in charge to use core modules, models and mutate the data are stored inside `api/services`.
- Explicit in-code documentation for parameters and responses for each endpoint (Inside the following paths `api/v1/controllers/v1/<resourceName>/<action>.js`).
- Explicit routes and path parameters inside `config/routes.js`
- By default, Sails uses a disk based database stored inside `./tmp` folder. All the models are mapped to disk automatically when running the application the first time. Data migrations are also automatically tracked by the ORM `Waterline`.
- The API has a versioned REST style, starting as version `v1`.
- This code implements basic rules plus:
    - Allow specific players to make plays
    - Player words tracking
    - Score route

### Testing. The strategy

In order to test this API properly, the more important thing is to properly separate the logic layer from web related layer (Routing logic, controllers logic, rendering logic) and also from data layer.

To do this it's crucial to define this core logic, module by module, its functions and signatures.

It should receive already obtained data as abstract variables. In the core we do not fetch the database, for example.

So, with this in mind, the strategy is the following:

- Test all the core logic layer with unit tests module by module.
- Test all the module interactions (services), without involving web logic, also with unit and integration tests when possible.
- Keep controller tests as trivial as success and failure cases, using test oriented helpers.
- Keep routing tests as trivial as success and failure cases.
- Add test coverage tools to make sure no relevant file is missing.

Whit this approach we win:

- Posibilty to change the entire implementation for the core logic or to easy undertand it.
- Posibilty to change the entire web layer if having environment issues, or if we need to change the responses serialization.
- Easier API extension, readability and maintainability.

## Install instructions (using nvm)

This app was built using node version 12.9.1, and provides a `.nvmrc` file to use with the `nvm use` command inside the root folder.

### Installing nvm

To intall `nvm` simple run the following (`curl` or `wget` are required):

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
```

or

```bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
```

Then you need to be sure that the following lines was added to four current terminal source files (Like `.bashrc` or `.zshrc`):

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

> For further nvm installation instructions go [here](https://github.com/nvm-sh/nvm).

### Installing node

To install node v12.9.1, run the following:

```bash
nvm install v12.9.1
```

And to set this version as default, run:

```bash
nvm alias default v12.9.1
```

### Installing app dependencies

With `node` already installed, run the following inside the project root directory:

```bash
npm install
```

### Starting application

To start the application just run:

```
sails lift
```

This will start a server listening port to `1337` in `localhost`.

> Visit the local server [here](http://localhost:1337).

## Test instructions

To test the application just run

```
npm test
```

The test script checks the entire application for syntax error or warnings using eslint (ecmaVersion 8), and then uses mocha to run test suit (The path for matching test files is defined inside `package.json` file).

## Endpoints explained

### POST /api/v1/dictionares

This endpoint creates a new dictionary (Append only). Internally it creates or use already created word records to avoid duplication.

Valid body example:

```json
{"name": "Weapons", "words": ["sword", "shield", "lance", "gun", "saw", "laser"]}
```

Example success response:

```json
{
  "words": [
    "SWORD",
    "SHIELD",
    "LANCE",
    "GUN",
    "SAW",
    "AXE",
    "LASER"
  ],
  "createdAt": 1571346900317,
  "updatedAt": 1571346900317,
  "id": 1,
  "name": "Weapons"
}
```

### POST /api/v1/boards?dictionaryId={id}

This endpoint creates a new game board, using the provided dictionary ID.

The board only needs an extra list of players, that its a simplified version of real logged in players, inside a more complex model.

Valid body example:

```json
{"players": ["kiel", "maguilar"], "dictionaryId": 1}
```

Example success response:

```json
{
  "players": [
    "kiel",
    "maguilar"
  ],
  "createdAt": 1571347158832,
  "updatedAt": 1571347158832,
  "id": 1,
  "status": "new",
  "winnerPlayer": null,
  "nextPlayer": "kiel",
  "score": {
    "kiel": 0,
    "maguilar": 0
  },
  "dictionaryId": 1,
  "missingWords": 3,
  "board": [
    [
      "L",
      "A",
      "N",
      "H"
    ],
    [
      "L",
      "C",
      "E",
      "S"
    ],
    [
      "W",
      "S",
      "N",
      "G"
    ],
    [
      "W",
      "A",
      "S",
      "U"
    ]
  ],
  "discoveredBoard": [
    [
      "[x]",
      "[x]",
      "[x]",
      "[x]"
    ],
    [
      "[x]",
      "[x]",
      "[x]",
      "[x]"
    ],
    [
      "[x]",
      "[x]",
      "[x]",
      "[x]"
    ],
    [
      "[x]",
      "[x]",
      "[x]",
      "[x]"
    ]
  ]
}
```

> `dictionaryId` can be provided inside request body, or as a query string parameter.

### GET /api/v1/boards/:boardId

This endpoints provides the current board status (`new` or `finished`), and its current score. It also provides discoverd words, and the next player allowed to move.

Example success response:

```json
{
  "players": [
    "kiel",
    "maguilar"
  ],
  "createdAt": 1571279657289,
  "updatedAt": 1571337476854,
  "id": 5,
  "status": "finished",
  "winnerPlayer": "maguilar",
  "nextPlayer": "kiel",
  "score": {
    "kiel": 2,
    "maguilar": 4
  },
  "dictionaryId": 1,
  "missingWords": 0,
  "board": [
    [
      "W",
      "S",
      "S",
      "4"
    ],
    [
      "O",
      "D",
      "E",
      "E"
    ],
    [
      "R",
      "A",
      "X",
      "C"
    ],
    [
      "L",
      "A",
      "N",
      "S"
    ]
  ],
  "discoveredBoard": [
    [
      "W",
      "S",
      "[x]",
      "4"
    ],
    [
      "O",
      "D",
      "E",
      "E"
    ],
    [
      "R",
      "A",
      "X",
      "C"
    ],
    [
      "L",
      "A",
      "N",
      "[x]"
    ]
  ]
}
```

### PATCH /api/v1/boards/:boardId

This endpoint allows players to make plays. When a play is valid, the game status moves on. If the play is not valid, the game only skip players turn. Board positions are integer numbers between 0 and 15 (Enumerating the board top to down and then left to right).

For the example board:

```
S  S  S  A
A  H  L  D
W  E  I  E
G  L  A  S
```

The valid positions are:

```
0  1  2  3
4  5  6  7
8  9  10 11
12 13 14 15
```

Valid body example:

```json
{"player": "maguilar", "positions": [0, 5, 10, 11, 6, 7]}
```

Example success response:

```json
{
  "players": [
    "kiel",
    "maguilar"
  ],
  "createdAt": 1571348015172,
  "updatedAt": 1571349327381,
  "id": 9,
  "status": "new",
  "winnerPlayer": null,
  "nextPlayer": "kiel",
  "score": {
    "kiel": 0,
    "maguilar": 8
  },
  "dictionaryId": 37,
  "missingWords": 1,
  "board": [
    [
      "S",
      "S",
      "S",
      "A"
    ],
    [
      "A",
      "H",
      "L",
      "D"
    ],
    [
      "W",
      "E",
      "I",
      "E"
    ],
    [
      "G",
      "L",
      "A",
      "S"
    ]
  ],
  "discoveredBoard": [
    [
      "S",
      "[x]",
      "[x]",
      "[x]"
    ],
    [
      "[x]",
      "H",
      "L",
      "D"
    ],
    [
      "[x]",
      "[x]",
      "I",
      "E"
    ],
    [
      "[x]",
      "[x]",
      "[x]",
      "[x]"
    ]
  ]
}
```

> The play discovered the word `SHIELD` that gives `maguilar` a score of `8` points.

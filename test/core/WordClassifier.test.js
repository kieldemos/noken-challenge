// ./test/core/WordClassifier.test.js
const WordClassifier = require('../../api/core/WordClassifier');

function notEqualsArrays(array1, array2){
  return JSON.stringify(array1) !== JSON.stringify(array2);
}

describe('WordClassifier (service)', () => {

  describe('#groupByLengthAndLetters(words)', () => {
    it('should build a Map with words by length', (done) => {
      const words = [
        'MACHINE', // length: 7
        'GUN', // length: 3
        'MAN', // length: 3
        'MEN', // length: 3
        'WEAPON' // length: 6
      ];

      const wordsByLength = WordClassifier.groupByLengthAndLetters(words);

      if (notEqualsArrays(['MAN', 'MEN'], wordsByLength.get(3).get('M'))) {
        return done(new Error('Should classify MAN and MEN inside length 3, letter M'));
      }

      if (notEqualsArrays(['GUN'], wordsByLength.get(3).get('G'))) {
        return done(new Error('Should classify GUN inside length 3, letter G'));
      }

      if (notEqualsArrays(['WEAPON'], wordsByLength.get(6).get('W'))) {
        return done(new Error('Should classify WEAPON inside length 6, letter W'));
      }

      if (notEqualsArrays(['MACHINE'], wordsByLength.get(7).get('M'))) {
        return done(new Error('Should classify MACHINE inside length 7, letter M'));
      }

      return done();
    });
  });

  describe('#removeWord(wordsByLength, word, wordLength)', () => {
    it('should properly remove a word inside classified Map', (done) => {
      var wordsByLength = new Map();

      var seven = new Map();
      seven.set('M', ['MACHINE']);
      wordsByLength.set(7, seven);

      var three = new Map();
      three.set('M', ['MAN', 'MEN']);
      three.set('G', ['GUN']);
      wordsByLength.set(3, three);

      var six = new Map();
      six.set('W', ['WEAPON']);
      wordsByLength.set(6, six);

      // Removing MEN
      WordClassifier.removeWord(wordsByLength, 'MEN', 3);

      if (notEqualsArrays(['MAN'], wordsByLength.get(3).get('M'))) {
        return done(new Error('Should remove MEN from level 3'));
      }

      // Removing MAN
      WordClassifier.removeWord(wordsByLength, 'MAN', 3);

      if (undefined !== wordsByLength.get(3).get('M')) {
        return done(new Error('Words starting with M at level 3 should been removed'));
      }

      if (notEqualsArrays(['GUN'], wordsByLength.get(3).get('G'))) {
        return done(new Error('GUN from level 3 should be kept'));
      }

      // Removing GUN
      WordClassifier.removeWord(wordsByLength, 'GUN', 3);

      if (undefined !== wordsByLength.get(3)) {
        return done(new Error('Level 3 should be removed'));
      }

      if (notEqualsArrays(['WEAPON'], wordsByLength.get(6).get('W'))) {
        return done(new Error('Should keep WEAPON inside length 6'));
      }

      if (notEqualsArrays(['MACHINE'], wordsByLength.get(7).get('M'))) {
        return done(new Error('Should keep MACHINE inside length 7'));
      }

      return done();
    });
  });
});
